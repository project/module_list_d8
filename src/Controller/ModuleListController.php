<?php

namespace Drupal\module_list_d8\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\ClientInterface;

/**
 * Module List Controller Class.
 */
class ModuleListController extends ControllerBase {

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Constructs a http object.
   *
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The Guzzle HTTP client.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    );
  }

  /**
   * List out custom and contrib modules in the site.
   *
   * @return table
   *   Module list table. Return the details of custom and contRibuted project.
   */
  public function modulelistreports() {
    $module_list = system_rebuild_module_data();
    $contrib_list = $custom_list = [];
    foreach ($module_list as $machine_name => $data) {

      $info = $data->info;

      $pack = array_key_exists('project', $info) ? $info['project'] : '';

      if ($pack != 'drupal' && $pack != 'libraries') {
        if ($pack == $machine_name) {
          $xml_value = $this->getListDrupalOrgCheck($machine_name);

          if (isset($xml_value) && $xml_value['mod_avl'] == 1) {
            $contrib_list[$machine_name]['data']['name'] = $data->info['name'];
            $contrib_list[$machine_name]['data']['machine_name'] = $machine_name;
            // Enabled or disabled?
            $contrib_list[$machine_name]['data']['status'] = $data->status == 1 ? 'Enabled' : 'Disabled';
            // Support Available?
            $contrib_list[$machine_name]['data']['support'] = $xml_value['project_status'] == 'published' ? 'Available' : 'Not Available';
            $contrib_list[$machine_name]['style'] = $xml_value['project_status'] == 'published' ? '' : 'background-color: #FD4242';
          }
          elseif (isset($xml_value) && $xml_value['mod_avl'] == 2) {
            $custom_list[$machine_name]['data']['name'] = $data->info['name'];
            $custom_list[$machine_name]['data']['machine_name'] = $machine_name;
            // Enabled or disabled?
            $custom_list[$machine_name]['data']['status'] = $data->status == 1 ? 'Enabled' : 'Disabled';
            // Support Available?
            $custom_list[$machine_name]['data']['support'] = 'Not Available';
          }
        }
      }
    }
    $style = ['width: 300px;', 'text-align: center;'];
    $count_header = [
      [
        'data' => 'Module count',
        'style' => ['text-align: center;'],
        'colspan' => 2,
      ],
    ];
    $count_row = [
      ['Custom Module Count', count($custom_list)],
      ['Contributed Module Count', count($contrib_list)],
    ];
    $list_header = [
      ['data' => 'Module Name', 'style' => $style],
      ['data' => 'Machine Name', 'style' => $style],
      ['data' => 'Status', 'style' => $style],
      ['data' => 'Support', 'style' => $style],
    ];
    $custom_header = [
      [
        'data' => 'list of Custom Modules',
        'style' => ['text-align:center;'],
      ],
    ];
    $contrib_header = [
      [
        'data' => 'list of Contributed Modules',
        'style' => ['text-align:center;'],
      ],
    ];

    $form['count_header'] = [
      '#type' => 'table',
      '#header' => $count_header,
      '#rows' => $count_row,
    ];
    $form['custom_header'] = [
      '#type' => 'table',
      '#header' => $custom_header,
    ];
    $form['list_header'] = [
      '#type' => 'table',
      '#header' => $list_header,
      '#rows' => $custom_list,
    ];
    $form['contrib_header'] = [
      '#type' => 'table',
      '#header' => $contrib_header,
    ];
    $form['list_contrib_header'] = [
      '#type' => 'table',
      '#header' => $list_header,
      '#rows' => $contrib_list,
    ];
    return $form;
  }

  /**
   * Generates the drupal.org URL to fetch information for a single project.
   *
   * @param string $machine_name
   *   Machine name of the project to fetch data for.
   *
   * @return array
   *   Return project details.
   */
  public function getListDrupalOrgCheck($machine_name) {
    $available = '';
    $drupal_version = \Drupal::CORE_COMPATIBILITY;
    $url = 'http://updates.drupal.org/release-history' . '/' . $machine_name . '/' . $drupal_version;

    try {
      $response = $this->httpClient->request('GET', $url, ['headers' => ['Accept' => 'text/plain']]);
      if ($response->getBody()) {
        $data = (string) $response->getBody();
      }
      if (!empty($data)) {
        $details = [];
        // Parses the XML of the Drupal release history info files.
        // Retruns array of parsed data about releases for a given project.
        $available = simplexml_load_string($data);

        if ($available->releases) {
          // Module Available in Drupal.org.
          $details['mod_avl'] = 1;
          // Project Status Supported or unsuppoted.
          $details['project_status'] = $available->project_status;
        }
        else {
          // Module not available in Drupal.org i.e custom module.
          $details['mod_avl'] = 2;
          // Marking it as custom module.
          $details['project_status'] = 'custom';
        }
        $details['machine_name'] = $machine_name;
        $this->messenger()->addStatus($this->t('Module report has been executed successfully'));
      }
      else {
        $details = NULL;
        $this->messenger()->addWarning($this->t('Your internet connection appears to be offline. Please check your internet connection'));
      }
    }
    catch (RequestException $e) {
      return FALSE;
    }
    return $details;
  }

}
