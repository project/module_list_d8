CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Hi drupallers,
Tired of knowing the list of custom modules in the site? Module list will
help you to list out the custom and contributed module list available
 in the site.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/module_list_d8

REQUIREMENTS
------------

 -No special requirements.

INSTALLATION
------------

1. Extract the module in the folder modules/contrib.

2. Go to the Module page at Administer > Extend
 (http://example.com/admin/modules) and enable it.

To view the list please go to Administer > Reports > All Module List

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.

MAINTAINERS
-----------

Current maintainers:
 * Purvita Gupta (purvitagupta) - hhttps://www.drupal.org/u/purvitagupta